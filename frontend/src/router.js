import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      components:{
        default: Home,
      }
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Login.vue")
    },
    {
      path: "/sign-up",
      name: "signup",
      component: () =>
          import(/* webpackChunkName: "about" */ "./views/SignUp.vue")
    },
    {
      path: "/selling-books",
      name: "Seller",
      component: () =>
          import(/* webpackChunkName: "about" */ "./views/BookSellRegistration.vue")
    },
    {
      path: "/sell/:id",
      name: "Sell Details",
      component: () =>
          import( "./views/BookDetails.vue")
    },
    {
      path: "/shopping-cart",
      name: "Shopping Cart",
      component: () =>
          import("./views/ShoppingCart.vue")
    },
    {
      path: "/order",
      name: "Order Number",
      component: () =>
          import("./views/Order.vue")
    }
  ]
});

