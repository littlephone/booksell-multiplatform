<?php


namespace App\Http\Controllers;

use App\Booksell\BookOrder;
use App\Booksell\APITools;
use Illuminate\Http\Request;

class OrderController
{
    /**
     * Place order and generate order IDs
     *
     * @param  Request  $request
     * @return Response
     */
    public function placeOrder(Request $request) {
        $connection = app()->database;
        $apitools = new APITools();
        if (!($items = $request->input('book_id'))) {
          $apitools->output([
              'error' => 'No book id is given',
                  ]
          );
        }
        $book_order = new BookOrder($connection);
        //We choose to create order first, but actually could be created automatically.
        $book_order->createOrder($_SESSION['id']);
        foreach ($items as $item) {
            $book_order->addItemToOrder($item);
        }

        $data['data']['success'] = true;
        $apitools->output($data);

    }

    public function getOrder(Request $request, int $order_id) {
        $connection = app()->database;
        $apitools = new APITools();
        $book_order = new BookOrder($connection);
        $apitools->output($book_order->getOrderById($order_id));

    }

    public function getMyOrders(Request $request) {
        $connection = app()->database;
        $apitools = new APITools();
        $book_order = new BookOrder($connection);
        $apitools->output($book_order->getAllOrdersById($_SESSION['id']));
    }

}