<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class AuthLoginUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SESSION)) session_start();
        if(!isset($_SESSION['id']) && !isset($_SESSION['username'])){
            abort(403, 'Access denied');
        }

        return $next($request);
    }
}
