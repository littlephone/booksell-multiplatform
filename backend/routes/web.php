<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'] , function() use ($router){
    global $connection;
    $connection = $router->app->database;

    $router->group(['prefix' => 'users'], function() use ($router){
        $router->get('check-login', function() {
            global $connection;
            return view('check-login');
        });
        $router->post('login', function() {
            global $connection;
            return view('login');
        });
        $router->post('logout', function() {
            global $connection;
            return view('logout');
        });

        $router->post('sign-up', function() {
            global $connection;
            return view('sign-up');
        });
    });

    $router->group(['prefix' => 'books'], function() use ($router){
        $router->post('sell-book', ['middleware' => [ 'verify_input' , 'auth_reg_user'],
            'uses' => 'BookListController@insert'
        ]);

        $router->post('listing/price-list', 'BookListController@getListOfPrices');
        $router->get('listing/{id}', 'BookListController@get');
    });

    $router->group(['prefix' => 'orders'], function() use ($router){
        $router->post('place-order', ['middleware' => ['auth_reg_user', 'check_book_availability'],
            'uses' => 'OrderController@placeOrder'
        ]);

        $router->get('me', ['middleware' => ['auth_reg_user'],
            'uses' => 'OrderController@getMyOrders'
        ]);
    });

    $router->get("image/{filename}", 'ImageController@get');

});




